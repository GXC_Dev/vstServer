Using the server:
To start running the server, navigate to the release folder and run server.exe
Then, once the server is running, it can be contacted through http at the specified port and ip(default is localhost 50051). Port and IP can be changed in the server code as specified by comments. The same applies for the Client examples. 
The calls that can be made to the server include:
/niRFSG/vstlist
Input: N/A
Expected Output: returns the list of currently connected niRFSG driven devices in string format, separated by '\n'

/niRFSA/vstlist
Input: N/A
Expected Output: returns the list of currently connected niRFSA driven devices in string format, separated by '\n'

/niRFSA/opensession
Input: JSON containing resourceName, idQuery, reset, and optionString
Expected Output: Opens a session for the given resourceName for niRFSA. Returns any error caused.

/niRFSG/opensession
Input: JSON containing resourceName, idQuery, reset, and optionString
Expected Output: Opens a session for the given resourceName for niRFSG. Returns any error caused.

/niRFSG/closesession
Input: JSON containing resourceName 
Expected Output: close a session for an niRFSG device that currently has an open session. Returns any error caused.

/niRFSA/closesession
Input: JSON containing resourceName 
Expected Output:close a session for an niRFSA device that currently has an open session. Returns any error caused.

/closeallsessions
Input: N/A
Expected Output: Closes all niRFSA and niRFSG sessions on a device

/niRFSA/abort
Input: JSON containing resourceName
Expected Output: Calls abort on the device session. Returns any error caused.

/niRFSG/abort
Input: JSON containing resourceName
Expected Output: Calls abort on the device session. Returns any error caused.

/niRFSA/initiate
Input: JSON containing resourceName
Expected Output: Calls initiate on the device session. Returns any error caused.

/niRFSG/initiate
Input: JSON containing resourceName
Expected Output: Calls initiate on the device session. Returns any error caused.

/niRFSG/configure
Input: JSON containing resourceName, along with any attributes and values to set, in the order they are to be set. 
Expected Output: Configures attributes sent in JSON to the corresponding values from the JSON. Returns any errors that may have occured. 

/niRFSA/configure
Input: JSON containing resourceName, along with any attributes and values to set, in the order they are to be set. 
Expected Output: Configures attributes sent in JSON to the corresponding values from the JSON. Returns any errors that may have occured.

/niRFSG/getconfiguration
Input: A JSON containing resourceName as well as a list of attributes, "attributes"
Expected Output: A JSON output containing all the specified attributes and their current values is returned.

/niRFSA/getconfiguration
Input: A JSON containing resourceName as well as a list of attributes, "attributes"
Expected Output: A JSON output containing all the specified attributes and their current values is returned.

/niRFSA/fetch
Input: JSON containing resourceName, channelName, startingRecord, numberOfRecords, numberOfSamples, timeout, and recordNumber. Depending on desired fetch call, not all parameters are necessary(see niRFSA.h for fetch calls and parameters). Attributes "type" and "dataType" can also be sent in to specify multi/single-record and the data type to be fetched. 
Expected Output: Returns waveform data as bytes paired in IQ format. (i.e. Client could read in data as a single list where even index values are the I values while odd indexed values are the Q values)

/niRFSG/writescript
Input: JSON containing resourceName and script.
Expected Output: Calls writeScript on an niRFSG device. Returns any error message that may occur in the call.

/niRFSG/write
Input: F32 bytes sent as I and then Q data. Header includes resourceName, waveformName, numberOfSamples, and moreDataPending. 
Expected Output: Writes the F32 IQ data to the VST. Returns any error message that may occur in the call. 

/niRFSA/read
Input: JSON containing resourceName, channelList, timeout. 
Expected Output: Writes F64 powerSpectrum data back to the client. 

/niRFSG/waituntilsettled
Input: JSON containing resourceName.
Expected Output: calls waitUntilSettled on the niRFSG device. Returns any error message that may occur in the call. 

*For more information on Ni function call parameters and what they mean, examine niRFSA.h, niRFSG.h, and/or NI's official documentation on the specific function as found on their website. 
Similarly, those three sources also can help users understand what certain values mean for certain parameters. For example, it can be seen in both niRFSA.h and in the online documentation that setting acquisitionType to 101 means setting it to Spectrum mode.
Examples of expected use can also be seen in niRFSA_Client.py and niRFSG_Client.py




Building from source:
There are multiple ways the exe can be built from source. Below is the description of building from MSVC with VCPKG:

Install vcpkg and add it to path

In command prompt run:
vcpkg install boost:x64-windows
vcpkg integrate install

Create a new project in MSVC with all source files. 
set mode to release instead of debug. Then, in project, properties, C/C++, General, set additional include directories to the directories containing niRFSA.h, niRFSG.h, and niModInst.h
Next, in Linker, General, set additional library directories to the directories containg the .lib files corresponding with the above headers.
Next, in Linker, Input, add visa32.lib; ivi.lib;niRFSA.lib;niRFSG.lib;niModInst.lib; to additional dependencies.  
"Search Everything" is a very useful tool to help find which directories contain the necessary files on your machine. 
You should then be able to comile and run the server. The exe should appear in the Release directory. 
When running the Server, the Windows firewall will pop up asking for permission. This permission must be allowed for the server to run. There may also be a missing .dll file in the release folder. If this is the case, find that file on the machine and add it to the folder. 

With regards to the python Clients, running is fairly standard.
Any dependencies needed can be pip installed(http.client, numpy, json, pyqt, pyqtgraph, etc).
They can then be called from command prompt or IDE. I used python 3.9 IDLE to write and run the code. 