#include "client_http.hpp"
#include "server_http.hpp"
#include <future>

#include "niRFSG.h"
#include "niRFSA.h"
#include "niModInst.h"
#include "visa.h"
//#include "ivi.h"

//#include <string>
#include "boost/tuple/tuple.hpp"



// Added for the json-example
#define BOOST_SPIRIT_THREADSAFE
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// Added for the default_resource example
#include <algorithm>
#include <boost/filesystem.hpp>
#include <fstream>
#include <vector>
#ifdef HAVE_OPENSSL
#include "crypto.hpp"
#endif


using namespace std;

using boost::tuple;

typedef vector<boost::tuple<string, ViSession, string>> tuple_list;
typedef std::map<string, std::pair<string, ViAttr> > attributeMap;

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
using HttpClient = SimpleWeb::Client<SimpleWeb::HTTP>;


//helper function for determining index of certain device session in the list:
int indexInOpenDevices(tuple_list openDevices, string name, string driver) {
    for (int i = 0; i < openDevices.size(); i++) {
        if (name.compare(openDevices.at(i).get<0>()) == 0 && driver.compare(openDevices.at(i).get<2>()) == 0) {
            return i;
        }
    }
    return -1;
}

int main() {
    // HTTP-server at port 50051 using 1 thread
    // Unless you do more heavy non-threaded processing in the resources,
    // 1 thread is usually faster than several threads
    HttpServer server;
    server.config.port = 50051;
    //Uncomment to set IP Address
    //server.config.address = "10.190.1.181";

    //data structure to hold all devices with their corresponding ViSessions
    tuple_list openDevices;

    //data structure to link all configure functions to their attribute values
    //maps the user-inputed attribute(in camelCase) to the data type and NI attribute. Used in Configure.
    //This does not account for every single attribute but it accounts for the majority of basic configure functions
    attributeMap niRFSA_Attributes;
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("acquisitionType", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_ACQUISITION_TYPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("referenceLevel", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_REFERENCE_LEVEL)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("iqCarrierFrequency", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_IQ_CARRIER_FREQUENCY)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("iqRate", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_IQ_RATE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("numberOfSamplesIsFinite", pair<string, ViAttr>("ViBoolean", NIRFSA_ATTR_NUMBER_OF_SAMPLES_IS_FINITE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("numberOfSamples", pair<string, ViAttr>("ViInt64", NIRFSA_ATTR_NUMBER_OF_SAMPLES)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("numberOfRecordsIsFinite", pair<string, ViAttr>("ViBoolean", NIRFSA_ATTR_NUMBER_OF_RECORDS_IS_FINITE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("numberOfRecords", pair<string, ViAttr>("ViInt64", NIRFSA_ATTR_NUMBER_OF_RECORDS)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("centerFrequency", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_CENTER_FREQUENCY)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("spectrumSpan", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_SPECTRUM_SPAN)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("resolutionBandwidth", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_RESOLUTION_BANDWIDTH)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("refClockSource", pair<string, ViAttr>("ViString", NIRFSA_ATTR_REF_CLOCK_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("refClockRate", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_REF_CLOCK_RATE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("pxiChassisClk10Source", pair<string, ViAttr>("ViString", NIRFSA_ATTR_PXI_CHASSIS_CLK10_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("startTriggerType", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_START_TRIGGER_TYPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeStartTriggerSource", pair<string, ViAttr>("ViString", NIRFSA_ATTR_DIGITAL_EDGE_START_TRIGGER_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeStartTriggerEdge", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_DIGITAL_EDGE_START_TRIGGER_EDGE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("refTriggerType", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_REF_TRIGGER_TYPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeRefTriggerSource", pair<string, ViAttr>("ViString", NIRFSA_ATTR_DIGITAL_EDGE_REF_TRIGGER_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeRefTriggerEdge", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_DIGITAL_EDGE_REF_TRIGGER_EDGE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("iqPowerEdgeRefTriggerSource", pair<string, ViAttr>("ViString", NIRFSA_ATTR_IQ_POWER_EDGE_REF_TRIGGER_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("iqPowerEdgeRefTriggerLevel", pair<string, ViAttr>("ViReal64", NIRFSA_ATTR_IQ_POWER_EDGE_REF_TRIGGER_LEVEL)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("iqPowerEdgeRefTriggerSlope", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_IQ_POWER_EDGE_REF_TRIGGER_SLOPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("refTriggerPretriggerSamples", pair<string, ViAttr>("ViInt64", NIRFSA_ATTR_REF_TRIGGER_PRETRIGGER_SAMPLES)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("advanceTriggerType", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_ADVANCE_TRIGGER_TYPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeAdvanceTriggerSource", pair<string, ViAttr>("ViString", NIRFSA_ATTR_DIGITAL_EDGE_ADVANCE_TRIGGER_SOURCE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("deembeddingType", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_DEEMBEDDING_TYPE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("deembeddingSelectedTable", pair<string, ViAttr>("ViString", NIRFSA_ATTR_DEEMBEDDING_SELECTED_TABLE)));
    niRFSA_Attributes.insert(pair<string, pair<string, ViAttr>>("numberOfSpectralLines", pair<string, ViAttr>("ViInt32", NIRFSA_ATTR_NUMBER_OF_SPECTRAL_LINES)));


    //data structure to link all configure functions to their attribute values
    //maps the user-inputed attribute(in camelCase) to the data type and NI attribute. Used in Configure.
    //This does not account for every single attribute but it accounts for the majority of basic configure functions
    attributeMap niRFSG_Attributes;
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("phaseOffset", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_PHASE_OFFSET)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("upconverterCenterFrequency", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_UPCONVERTER_CENTER_FREQUENCY)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("frequency", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_FREQUENCY)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("powerLevel", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_POWER_LEVEL)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("powerLevelType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_POWER_LEVEL_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("outputEnabled", pair<string, ViAttr>("ViBoolean", NIRFSG_ATTR_OUTPUT_ENABLED)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("signalBandwidth", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_SIGNAL_BANDWIDTH)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("refClockRate", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_REF_CLOCK_RATE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("refClockSource", pair<string, ViAttr>("ViString", NIRFSG_ATTR_REF_CLOCK_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("pxiClk10Source", pair<string, ViAttr>("ViString", NIRFSG_ATTR_PXI_CHASSIS_CLK10_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("generationMode", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_GENERATION_MODE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeStartTriggerEdge", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_EDGE_START_TRIGGER_EDGE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeStartTriggerSource", pair<string, ViAttr>("ViString", NIRFSG_ATTR_DIGITAL_EDGE_START_TRIGGER_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("p2pEndpointFullnessStartTriggerLevel", pair<string, ViAttr>("ViInt64", NIRFSG_ATTR_P2P_ENDPOINT_FULLNESS_START_TRIGGER_LEVEL)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("startTriggerType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_START_TRIGGER_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeConfigurationListStepTriggerSource", pair<string, ViAttr>("ViString", NIRFSG_ATTR_DIGITAL_EDGE_CONFIGURATION_LIST_STEP_TRIGGER_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeConfigurationListStepTriggerEdge", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_EDGE_CONFIGURATION_LIST_STEP_TRIGGER_EDGE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeScriptTriggerEdge", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_EDGE_SCRIPT_TRIGGER_EDGE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalEdgeScriptTriggerSource", pair<string, ViAttr>("ViString", NIRFSG_ATTR_DIGITAL_EDGE_SCRIPT_TRIGGER_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalLevelScriptTriggerActiveLevel", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_LEVEL_SCRIPT_TRIGGER_ACTIVE_LEVEL)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalLevelScriptTriggerSource", pair<string, ViAttr>("ViString", NIRFSG_ATTR_DIGITAL_LEVEL_SCRIPT_TRIGGER_SOURCE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("scriptTriggerType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_SCRIPT_TRIGGER_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_MODULATION_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationSymbolRate", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_DIGITAL_MODULATION_SYMBOL_RATE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationWaveformType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_MODULATION_WAVEFORM_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationPrbsOrder", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_MODULATION_PRBS_ORDER)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationPrbsSeed", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DIGITAL_MODULATION_PRBS_SEED)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("digitalModulationFskDeviation", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_DIGITAL_MODULATION_FSK_DEVIATION)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("deembeddingType", pair<string, ViAttr>("ViInt32", NIRFSG_ATTR_DEEMBEDDING_TYPE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("deembeddingSelectedTable", pair<string, ViAttr>("ViString", NIRFSG_ATTR_DEEMBEDDING_SELECTED_TABLE)));
    niRFSG_Attributes.insert(pair<string, pair<string, ViAttr>>("deembeddingCompensationGain", pair<string, ViAttr>("ViReal64", NIRFSG_ATTR_DEEMBEDDING_COMPENSATION_GAIN)));


    // POST-example for the path /postname, responds the posted string
    server.resource["^/postname$"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        // Retrieve string:
        auto content = request->content.string();

        //*response << "HTTP/1.1 200 OK\r\nContent-Length: " << content.length() << "\r\n\r\n"
        //          << content;
        response->write(content + "Sending was successful");
    };


    // GET-example for the path /getname, responds with the sent string
    server.resource["^/getname$"]["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        auto content = request->content.string();
        response->write(content + "Sending was successful");
        //response->write(12);
        //response->write(request->query_string);
        //response->write(request->content.str());
    };



    // POST-example for the path /json, responds firstName+" "+lastName from the posted json
    // Responds with an appropriate error message if the posted json is not valid, or if firstName or lastName is missing
    // Example posted json:
    // {
    //   "firstName": "John",
    //   "lastName": "Smith",
    //   "age": 25
    // }
    server.resource["^/simplejson$"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            boost::property_tree::ptree pt;
            read_json(request->content, pt);
            std::stringstream ss;
            boost::property_tree::json_parser::write_json(ss, pt);
            //auto name = pt.get<string>("firstName") + " " + pt.get<string>("lastName");

            response->write(ss);
            //response->write(request->content);
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    server.resource["^/testbinary$"]["POST"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //determine number of samples from header
            stringstream numSamples(request->header.find("numberOfSamples")->second);
            int numberOfSamples = 0;
            numSamples >> numberOfSamples;


            //read in f32 bytes into allocated array from streambuffer
            streambuf* buf = request->content.rdbuf();
            float* floats = new float[numberOfSamples];
            (*buf).sgetn(reinterpret_cast<char*>(floats), sizeof(float)* numberOfSamples);

            
            //write back to the client
            *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(float)*numberOfSamples << "\r\n\r\n";
            response->write((char*)floats, sizeof(float)* numberOfSamples);

            //clear allocated memory
            delete[] floats;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    //returns the list of currently connected niRFSG driven devices in string format, separated by '\n'
    server.resource["^/niRFSG/vstlist$"]["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            ViConstString driver = "niRFSG";
            ViSession vi;
            ViInt32 deviceCount;

            ViInt32 attributeId = NIMODINST_ATTR_DEVICE_NAME;
            string vstlist;

            //determine number of devices
            viCheckWarn(niModInst_OpenInstalledDevicesSession(driver, &vi, &deviceCount));

            //loop through every device and determine the device name and add it to the response
            for (int i = 0; i < deviceCount; i++) {
                //Determine size of device name by passing NULL as attributeValue char array
                int bufferSize = niModInst_GetInstalledDeviceAttributeViString(vi, i, attributeId, 0, NULL);

                //allocate memory to store vst name for current index
                ViChar* attributeValue = new ViChar[bufferSize];

                viCheckWarn(niModInst_GetInstalledDeviceAttributeViString(vi, i, attributeId, bufferSize, attributeValue));
                //add name of vst to vstlist with endline character
                vstlist = vstlist + attributeValue + "\n";

                //clear up memory allocated for storing name of vst
                delete[] attributeValue;
            }
            //remove last endline character
            vstlist = vstlist.substr(0, vstlist.length() - 1);

            Error:
            response->write(vstlist);
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };

    //returns the list of currently connected niRFSA driven devices in string format, separated by '\n'
    server.resource["^/niRFSA/vstlist$"]["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            ViConstString driver = "niRFSA";
            ViSession vi;
            ViInt32 deviceCount;

            ViInt32 attributeId = NIMODINST_ATTR_DEVICE_NAME;
            string vstlist;

            //determine number of devices
            viCheckWarn(niModInst_OpenInstalledDevicesSession(driver, &vi, &deviceCount));


            //loop through every device and determine the device name and add it to the response
            for (int i = 0; i < deviceCount; i++) {
                //Determine size of device name by passing NULL as attributeValue char array
                int bufferSize = niModInst_GetInstalledDeviceAttributeViString(vi, i, attributeId, 0, NULL);

                //allocate memory to store vst name for current index
                ViChar* attributeValue = new ViChar[bufferSize];

                viCheckWarn(niModInst_GetInstalledDeviceAttributeViString(vi, i, attributeId, bufferSize, attributeValue));
                //add name of vst to vstlist with endline character
                vstlist = vstlist + attributeValue + "\n";

                //clear up memory allocated for storing name of vst
                delete[] attributeValue;
            }
            //remove last endline character
            vstlist = vstlist.substr(0, vstlist.length() - 1);

            Error:
            response->write(vstlist);
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };



    //Opens a session for the given resourceName for niRFSA. Returns the status of opening a session
    server.resource["^/niRFSA/opensession$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        /*try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");
            char* name = new char[n.length() + 1];
            n.copy(name, n.size() + 1);
            name[n.size()] = '\0';

            //determine if there is a optionString
            string optionString = pt.get<string>("optionString", "");

            //read in remaining data from the JSON
            ViString srcName = (ViString)n.c_str();
            ViBoolean idQuery = pt.get<bool>("idQuery");
            ViBoolean reset = pt.get<bool>("reset");
            ViSession vi = VI_NULL;

            //initialize a session
            viCheckWarn(niRFSA_InitWithOptions(srcName, idQuery, reset, optionString.c_str(), &vi));


            //make output on server indicating session was opened
            cout << "Session opened on: " << name << endl;

            //save session handle in vector of tuples
            if (indexInOpenDevices(openDevices, name, "niRFSA") == -1) {
                openDevices.push_back(boost::tuple<string, ViSession, string>(name, vi, "niRFSA"));
            }else{
                openDevices.erase(openDevices.begin() + indexInOpenDevices(openDevices, name, "niRFSA") - 1);
                openDevices.push_back(boost::tuple<string, ViSession, string>(name, vi, "niRFSA"));
            }

            Error:
                //write the error message received
                ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
                ViChar* errorDescription = new ViChar[buf];
                niRFSA_GetError(vi, &error, buf, errorDescription);
                response->write(errorDescription);
                delete[] errorDescription;


            //clear allocated memory
            delete[] name;
        }*/
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");
            char* name = new char[n.length() + 1];
            n.copy(name, n.size() + 1);
            name[n.size()] = '\0';

            //determine if there is a optionString
            string optionString = pt.get<string>("optionString", "");

            //read in remaining data from the JSON
            ViString srcName = name;
            ViBoolean idQuery = pt.get<bool>("idQuery");
            ViBoolean reset = pt.get<bool>("reset");
            ViSession vi;

            //save session handle in vector of tuples
            if (indexInOpenDevices(openDevices, name, "niRFSA") == -1) {
                //initialize a session
                viCheckWarn(niRFSA_InitWithOptions(srcName, idQuery, reset, optionString.c_str(), &vi));
                openDevices.push_back(boost::tuple<string, ViSession, string>(name, vi, "niRFSA"));
            }
            else {
                vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();
            }

            //make output on server indicating session was opened
            cout << "Session opened on: " << name << endl;

        Error:
            //write the error message received
            ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSA_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;


            //clear allocated memory
            delete[] name;
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    //Opens a session for the given resourceName for niRFSG. Returns the status code of opening a session. 0 means success. >0 means warnings. <0 means errors.
    server.resource["^/niRFSG/opensession$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");
            char* name = new char[n.length() + 1];
            n.copy(name, n.size() + 1);
            name[n.size()] = '\0';

            //determine if there is a optionString
            string optionString = pt.get<string>("optionString", "");

            //read in remaining data from the JSON
            ViString srcName = name;
            ViBoolean idQuery = pt.get<bool>("idQuery");
            ViBoolean reset = pt.get<bool>("reset");
            ViSession vi;

            //save session handle in vector of tuples
            if (indexInOpenDevices(openDevices, name, "niRFSG") == -1) {
                //initialize a session
                viCheckWarn(niRFSG_InitWithOptions(srcName, idQuery, reset, optionString.c_str(), &vi));
                openDevices.push_back(boost::tuple<string, ViSession, string>(name, vi, "niRFSG"));
            }
            else {
                vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();
            }

            //make output on server indicating session was opened
            cout << "Session opened on: " << name << endl;

            Error:
                //write the error message received
                ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
                ViChar* errorDescription = new ViChar[buf];
                niRFSG_GetError(vi, &error, buf, errorDescription);
                response->write(errorDescription);
                delete[] errorDescription;


            //clear allocated memory
            delete[] name;
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    //close a session for an niRFSG device that currently has an open session
    server.resource["^/niRFSG/closesession$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            //initialize a session
            viCheckWarn(niRFSG_close(vi));

            //remove session from tuple list
            if (indexInOpenDevices(openDevices, n, "niRFSG") != -1) {
                openDevices.erase(openDevices.begin() + indexInOpenDevices(openDevices, n, "niRFSG"));
            }

            //make output on server indicating session was opened
            cout << "Session closed on: " << n << endl;

            Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };

    //close a session for an niRFSA device that currently has an open session
    server.resource["^/niRFSA/closesession$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            //initialize a session
            //ViStatus status = niRFSA_close(vi);
            viCheckWarn(niRFSA_close(vi));

            //remove session from tuple list
            if (indexInOpenDevices(openDevices, n, "niRFSA") != -1) {
                openDevices.erase(openDevices.begin() + indexInOpenDevices(openDevices, n, "niRFSA"));
            }

            //make output on server indicating session was opened
            cout << "Session closed on: " << n << endl;

            Error:
            //write the error message received
            ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSA_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };

    

    //close all sessions that currently have an open session
    server.resource["^/closeallsessions$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //close sessions
            for (int i = 0; i < openDevices.size(); i++) {
                ViSession vi = openDevices.at(i).get<1>();
                //determine which driver to close on
                if (openDevices.at(i).get<2>().compare("niRFSG") == 0) {
                    viCheckWarn(niRFSG_close(vi));
                }
                else {
                    viCheckWarn(niRFSA_close(vi));
                }
            }

            //clear openDevices list since all sessions are now closed.
            openDevices.clear();

            //make output on server indicating sessions were closed
            cout << "closed all sessions" << endl;

            Error:
            //return the status code
            response->write(to_string(((long)error)));

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };



    //abort for an niRFSA device
    server.resource["^/niRFSA/abort$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            //abort session
            viCheckWarn(niRFSA_Abort(vi));

            Error:
            //write the error message received
            ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSA_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    //abort for an niRFSG device
    server.resource["^/niRFSG/abort$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            //initialize a session
            viCheckWarn(niRFSG_Abort(vi));

            Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    //Initiate an niRFSA device
    server.resource["^/niRFSA/initiate$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            //initialize a session
            viCheckWarn(niRFSA_Initiate(vi));

        Error:
            //write the error message received
            ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSA_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    //initiate an niRFSG device
    server.resource["^/niRFSG/initiate$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            //initialize a session
            viCheckWarn(niRFSG_Initiate(vi));

        Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    //Does not account for channel Name
    server.resource["^/niRFSG/configure$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();


            //loop through all values in the property tree and configure accordingly:
            for (auto& it : pt) {
                if (it.first.compare("resourceName") != 0) {
                    auto iterator = niRFSG_Attributes.find(it.first);
                    if (iterator == niRFSG_Attributes.end()) {
                        response->write("Error: Attribute: " + it.first + " was not found");
                    }
                    else {
                        ViAttr attribute = (iterator->second).second;
                        if (((iterator->second).first).compare("ViReal64") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViReal64(vi, VI_NULL, attribute, pt.get<double>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViInt64") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViInt64(vi, VI_NULL, attribute, pt.get<int>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViInt32") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViInt32(vi, VI_NULL, attribute, pt.get<int>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViString") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViString(vi, VI_NULL, attribute, pt.get<string>(it.first).c_str()));
                        }
                        else if (((iterator->second).first).compare("ViSession") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViSession(vi, VI_NULL, attribute, pt.get<long>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViBoolean") == 0) {
                            viCheckWarn(niRFSG_SetAttributeViBoolean(vi, VI_NULL, attribute, pt.get<bool>(it.first)));
                        }
                    }
                }

            }

        Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };

    //Does not handle channel name
    server.resource["^/niRFSA/configure$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();


            //loop through all values in the property tree and configure accordingly:
            for (auto& it : pt) {
                if (it.first.compare("resourceName") != 0) {
                    auto iterator = niRFSA_Attributes.find(it.first);
                    if (iterator == niRFSA_Attributes.end()) {
                        response->write("Error: Attribute: " + it.first + " was not found");
                    }
                    else {
                        ViAttr attribute = (iterator->second).second;
                        if (((iterator->second).first).compare("ViReal64") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViReal64(vi, VI_NULL, attribute, pt.get<double>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViInt64") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViInt64(vi, VI_NULL, attribute, pt.get<int>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViInt32") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViInt32(vi, VI_NULL, attribute, pt.get<int>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViString") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViString(vi, VI_NULL, attribute, pt.get<string>(it.first).c_str()));
                        }
                        else if (((iterator->second).first).compare("ViSession") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViSession(vi, VI_NULL, attribute, pt.get<long>(it.first)));
                        }
                        else if (((iterator->second).first).compare("ViBoolean") == 0) {
                            viCheckWarn(niRFSA_SetAttributeViBoolean(vi, VI_NULL, attribute, pt.get<bool>(it.first)));
                        }
                    }
                }

            }

        Error:
            //write the error message received
            ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSA_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    server.resource["^/niRFSG/getconfiguration$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            //read in the JSON list of values
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //declare property tree and stringstream used to return json of attributes
            boost::property_tree::ptree returnPt;
            std::stringstream ss;

            //determine resource name
            string n = pt.get<string>("resourceName");
            

            std::vector<string> desiredAttributes;
            for (auto& attribute : pt.get_child("attributes")) {
                desiredAttributes.push_back(attribute.second.get_value<string>());
            }

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            for (string attribute : desiredAttributes) {
                auto iterator = niRFSG_Attributes.find(attribute);
                if (iterator == niRFSG_Attributes.end()) {
                    response->write("Error: Attribute: " + attribute + " was not found");
                }
                else {
                    ViAttr getAttribute = (iterator->second).second;
                    if (((iterator->second).first).compare("ViReal64") == 0) {
                        ViReal64 value = 0;
                        viCheckWarn(niRFSG_GetAttributeViReal64(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViInt64") == 0) {
                        ViInt64 value;
                        viCheckWarn(niRFSG_GetAttributeViInt64(vi, VI_NULL,getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViInt32") == 0) {
                        ViInt32 value;
                        viCheckWarn(niRFSG_GetAttributeViInt32(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViString") == 0) {
                        //obtain the buffer size
                        ViInt32 buffer = niRFSG_GetAttributeViString(vi, VI_NULL, getAttribute, 0, VI_NULL);
                        ViChar* value = new ViChar[buffer];
                        viCheckWarn(niRFSG_GetAttributeViString(vi, VI_NULL, getAttribute, buffer, value));
                        returnPt.put(attribute, value);
                        delete[] value;
                    }
                    else if (((iterator->second).first).compare("ViSession") == 0) {
                        ViSession value;
                        viCheckWarn(niRFSG_GetAttributeViSession(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViBoolean") == 0) {
                        ViBoolean value;
                        viCheckWarn(niRFSG_GetAttributeViBoolean(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                }
            }

            boost::property_tree::json_parser::write_json(ss, returnPt);

        Error:
            //return the status code
            if (error == 0) {
                //write the stringstream of configuration data
                response->write(ss);
            }
            else {
                //write the error message received
                ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
                ViChar* errorDescription = new ViChar[buf];
                niRFSG_GetError(vi, &error, buf, errorDescription);
                response->write(errorDescription);
                delete[] errorDescription;
            }

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    server.resource["^/niRFSA/getconfiguration$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            //read in the JSON list of values
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //declare property tree and stringstream used to return json of attributes
            boost::property_tree::ptree returnPt;
            std::stringstream ss;

            //determine resource name
            string n = pt.get<string>("resourceName");


            std::vector<string> desiredAttributes;
            for (auto& attribute : pt.get_child("attributes")) {
                desiredAttributes.push_back(attribute.second.get_value<string>());
            }

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            for (string attribute : desiredAttributes) {
                auto iterator = niRFSA_Attributes.find(attribute);
                if (iterator == niRFSA_Attributes.end()) {
                    response->write("Error: Attribute: " + attribute + " was not found");
                }
                else {
                    ViAttr getAttribute = (iterator->second).second;
                    if (((iterator->second).first).compare("ViReal64") == 0) {
                        ViReal64 value = 0;
                        viCheckWarn(niRFSA_GetAttributeViReal64(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViInt64") == 0) {
                        ViInt64 value;
                        viCheckWarn(niRFSA_GetAttributeViInt64(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViInt32") == 0) {
                        ViInt32 value;
                        viCheckWarn(niRFSA_GetAttributeViInt32(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViString") == 0) {
                        //obtain the buffer size
                        ViInt32 buffer = niRFSA_GetAttributeViString(vi, VI_NULL, getAttribute, 0, VI_NULL);
                        ViChar* value = new ViChar[buffer];
                        viCheckWarn(niRFSA_GetAttributeViString(vi, VI_NULL, getAttribute, buffer, value));
                        returnPt.put(attribute, value);
                        delete[] value;
                    }
                    else if (((iterator->second).first).compare("ViSession") == 0) {
                        ViSession value;
                        viCheckWarn(niRFSA_GetAttributeViSession(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                    else if (((iterator->second).first).compare("ViBoolean") == 0) {
                        ViBoolean value;
                        viCheckWarn(niRFSA_GetAttributeViBoolean(vi, VI_NULL, getAttribute, &value));
                        returnPt.put(attribute, value);
                    }
                }
            }

            boost::property_tree::json_parser::write_json(ss, returnPt);

        Error:
            //return the status code
            if (error == 0) {
                //write the stringstream of configuration data
                response->write(ss);
            }
            else {
                //write the error message received
                ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
                ViChar* errorDescription = new ViChar[buf];
                niRFSA_GetError(vi, &error, buf, errorDescription);
                response->write(errorDescription);
                delete[] errorDescription;
            }

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };



    //returns waveform data as bytes paired in IQ format.
    //(i.e. data could be read in as single list and even index values are the I values while odd indexed values are the Q values)
    server.resource["^/niRFSA/fetch$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;

            //read json into boost property tree
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //read in the data from the json into the appropriate variables. whichever variables are available will determine which fetch function to use.
            ViConstString channelList = (ViString)pt.get<string>("channelList", "").c_str();
            ViInt64 startingRecord = pt.get<int>("startingRecord", 0);
            ViInt64 numberOfRecords = pt.get<int>("numberOfRecords", 0);
            ViInt64 numberOfSamples = pt.get<int>("numberOfSamples", 0);
            ViReal64 timeout = pt.get<float>("timeout", 0);
            ViInt64 recordNumber = pt.get<int>("recordNumber", 0);


            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            //determine whether it is singleRecord or multiRecord
            string type = pt.get<string>("type", "multiRecord");

            //determine whether it is a I16, F32, or F64
            string dataType = pt.get<string>("dataType", "F32");


            niRFSA_wfmInfo* wfmInfo = new niRFSA_wfmInfo[numberOfRecords];

            int size = numberOfRecords * numberOfSamples;

            if (dataType.compare("F64") == 0) {
                if (type.compare("multiRecord") == 0) {
                    NIComplexNumber* data = new NIComplexNumber[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQMultiRecordComplexF64(vi, channelList, startingRecord, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexNumber)*size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexNumber)* size);
                    delete[] data;
                }
                else if (type.compare("singleRecord") == 0) {
                    NIComplexNumber* data = new NIComplexNumber[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQSingleRecordComplexF64(vi, channelList, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexNumber) * size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexNumber)* size);
                    delete[] data;
                }
                else {
                    response->write("Error: parameter: " + type + " not expected. Enter multiRecord or singleRecord");
                }
            }
            else if (dataType.compare("F32") == 0) {
                if (type.compare("multiRecord") == 0) {
                    NIComplexNumberF32* data = new NIComplexNumberF32[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQMultiRecordComplexF32(vi, channelList, startingRecord, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexNumberF32) * size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexNumberF32) * size);
                    delete[] data;
                }
                else if (type.compare("singleRecord") == 0) {
                    NIComplexNumberF32* data = new NIComplexNumberF32[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQSingleRecordComplexF32(vi, channelList, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexNumberF32) * size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexNumberF32) * size);
                    delete[] data;
                }
                else {
                    response->write("Error: parameter: " + type + " not expected. Enter multiRecord or singleRecord");
                }

            }
            else if (dataType.compare("I16") == 0) {
                if (type.compare("multiRecord") == 0) {
                    NIComplexI16* data = new NIComplexI16[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQMultiRecordComplexI16(vi, channelList, startingRecord, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexI16) * size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexI16) * size);
                    delete[] data;
                }
                else if (type.compare("singleRecord") == 0) {
                    NIComplexI16* data = new NIComplexI16[numberOfRecords * numberOfSamples];
                    viCheckWarn(niRFSA_FetchIQSingleRecordComplexI16(vi, channelList, numberOfRecords, numberOfSamples, timeout, data, wfmInfo));
                    //send data back to server
                    *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(NIComplexI16) * size << "\r\n\r\n";
                    response->write((char*)data, sizeof(NIComplexI16) * size);
                    delete[] data;
                }
                else {
                    response->write("Error: parameter: " + type + " not expected. Enter multiRecord or singleRecord");
                }
            }
            else {
                response->write("Error: Data type: " + dataType + " not expected. Enter data type F64, F32, or I16.");
            }

            delete[] wfmInfo;

            Error:
            //return the status code
                if (error != 0) {
                    //write the error message received
                    ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
                    ViChar* errorDescription = new ViChar[buf];
                    niRFSA_GetError(vi, &error, buf, errorDescription);
                    response->write(errorDescription);
                    delete[] errorDescription;
                }
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };



    //calls writeScript on an niRFSG device
    server.resource["^/niRFSG/writescript$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            //determine script
            string script = pt.get<string>("script");

            //call WriteScript
            viCheckWarn(niRFSG_WriteScript(vi, script.c_str()));

        Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };


    //calls write on an niRFSG device
    //expects data to be sent in order of i and then q data consecutively
    //F32 bytes
    server.resource["^/niRFSG/write$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //determine resourceName from header
            string resourceName = request->header.find("resourceName")->second;

            //determine waveformName from header
            string waveformName = request->header.find("waveformName")->second;

            //determine number of samples from header
            stringstream numSamples(request->header.find("numberOfSamples")->second);
            int numberOfSamples = 0;
            numSamples >> numberOfSamples;

            //determine moreDataPending
            stringstream moreData(request->header.find("moreDataPending")->second);
            bool moreDataPending = 0;
            moreData >> moreDataPending;

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, resourceName, "niRFSG")).get<1>();

            //read in f32 bytes into allocated array from streambuffer
            streambuf* buf = request->content.rdbuf();
            float* iq = new float[2*numberOfSamples];
            (*buf).sgetn(reinterpret_cast<char*>(iq), sizeof(float)* numberOfSamples*2);

            //call WriteArbWaveformF32 with I and Q data
            viCheckWarn(niRFSG_WriteArbWaveformF32(vi, waveformName.c_str(), numberOfSamples, iq, iq + numberOfSamples - 1, moreDataPending));

        Error:
            //write the error message received
            ViInt32 buff = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buff];
            niRFSG_GetError(vi, &error, buff, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    //calls readSpectrum on an niRFSA device
    //F64 bytes
    server.resource["^/niRFSA/read$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSA")).get<1>();

            //determine channelList
            string channelList = pt.get<string>("channelList", "");

            //determine timeout
            ViReal64 timeout = pt.get<float>("timeout", 0);


            //determine dataArraySize
            ViReal64* powerSpectrumData = VI_NULL;
            niRFSA_spectrumInfo* spectrumInfo = VI_NULL;
            ViInt32 size = 0;
            viCheckWarn(niRFSA_GetNumberOfSpectralLines(vi, channelList.c_str(), &size));

            powerSpectrumData = new ViReal64[size];
            spectrumInfo = new niRFSA_spectrumInfo[size];
            //niRFSA_spectrumInfo* spectrumInfo;

            //read Power Spectrum
            viCheckWarn(niRFSA_ReadPowerSpectrumF64(vi, channelList.c_str(), timeout, powerSpectrumData, size, VI_NULL));


            //write data back to client
            *response << "HTTP/1.1 200 OK\r\nContent-Length: " << sizeof(ViReal64) * size << "\r\n\r\n";
            response->write((char*)powerSpectrumData, sizeof(ViReal64)* size);


        Error:
            //return the status code
            if (error != 0) {
                //write the error message received
                ViInt32 buf = niRFSA_GetError(vi, VI_NULL, 0, VI_NULL);
                ViChar* errorDescription = new ViChar[buf];
                niRFSA_GetError(vi, &error, buf, errorDescription);
                response->write(errorDescription);
                delete[] errorDescription;
            }
        delete[] powerSpectrumData;
        delete[] spectrumInfo;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };



    //calls waituntilsettled on an niRFSG device
    server.resource["^/niRFSG/waituntilsettled$"]["POST"] = [&](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            //declare error variable for viCheckWarn
            ViStatus error = VI_SUCCESS;


            //read in the JSON
            boost::property_tree::ptree pt;
            read_json(request->content, pt);

            //determine resource name
            string n = pt.get<string>("resourceName");

            //Determine session from resource name
            ViSession vi = openDevices.at(indexInOpenDevices(openDevices, n, "niRFSG")).get<1>();

            //determine maxTimeMilliseconds
            int maxTimeMs = pt.get<int>("maxTimeMilliseconds");
            ViInt32 maxTimeMilliseconds = maxTimeMs;

            //call WaitUntilSettled with the given time(ms). 
            viCheckWarn(niRFSG_WaitUntilSettled(vi, maxTimeMilliseconds));

        Error:
            //write the error message received
            ViInt32 buf = niRFSG_GetError(vi, VI_NULL, 0, VI_NULL);
            ViChar* errorDescription = new ViChar[buf];
            niRFSG_GetError(vi, &error, buf, errorDescription);
            response->write(errorDescription);
            delete[] errorDescription;

        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, e.what());
        }
    };




    // Default GET-example. If no other matches, this anonymous function will be called.
    // Will respond with content in the web/-directory, and its subdirectories.
    // Default file: index.html
    // Can for instance be used to retrieve an HTML 5 client that uses REST-resources on this server
    server.default_resource["GET"] = [](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
        try {
            auto web_root_path = boost::filesystem::canonical("web");
            auto path = boost::filesystem::canonical(web_root_path / request->path);
            // Check if path is within web_root_path
            if (distance(web_root_path.begin(), web_root_path.end()) > distance(path.begin(), path.end()) ||
                !equal(web_root_path.begin(), web_root_path.end(), path.begin()))
                throw invalid_argument("path must be within root path");
            if (boost::filesystem::is_directory(path))
                path /= "index.html";

            SimpleWeb::CaseInsensitiveMultimap header;


            auto ifs = make_shared<ifstream>();
            ifs->open(path.string(), ifstream::in | ios::binary | ios::ate);

            if (*ifs) {
                auto length = ifs->tellg();
                ifs->seekg(0, ios::beg);

                header.emplace("Content-Length", to_string(length));
                response->write(header);

                // Trick to define a recursive function within this scope (for example purposes)
                class FileServer {
                public:
                    static void read_and_send(const shared_ptr<HttpServer::Response>& response, const shared_ptr<ifstream>& ifs) {
                        // Read and send 128 KB at a time
                        static vector<char> buffer(131072); // Safe when server is running on one thread
                        streamsize read_length;
                        if ((read_length = ifs->read(&buffer[0], static_cast<streamsize>(buffer.size())).gcount()) > 0) {
                            response->write(&buffer[0], read_length);
                            if (read_length == static_cast<streamsize>(buffer.size())) {
                                response->send([response, ifs](const SimpleWeb::error_code& ec) {
                                    if (!ec)
                                        read_and_send(response, ifs);
                                    else
                                        cerr << "Connection interrupted" << endl;
                                    });
                            }
                        }
                    }
                };
                FileServer::read_and_send(response, ifs);
            }
            else
                throw invalid_argument("could not read file");
        }
        catch (const exception& e) {
            response->write(SimpleWeb::StatusCode::client_error_bad_request, "Could not open path " + request->path + ": " + e.what());
        }
    };

    server.on_error = [](shared_ptr<HttpServer::Request> /*request*/, const SimpleWeb::error_code& /*ec*/) {
        // Handle errors here
        // Note that connection timeouts will also call this handle with ec set to SimpleWeb::errc::operation_canceled
    };

    // Start server and receive assigned port when server is listening for requests
    promise<unsigned short> server_port;
    thread server_thread([&server, &server_port]() {
        // Start server
        server.start([&server_port](unsigned short port) {
            server_port.set_value(port);
            });
        });
    cout << "Server listening on port " << server_port.get_future().get() << endl
        << endl;

    server_thread.join();
}
