import sys
import http.client
import json
import numpy

# Import QApplication and the required widgets from PyQt5.QtWidgets
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QLabel
from functools import partial
from PyQt5.QtCore import QTimer,QDateTime

#plotting tools
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg


ip = 'localhost'
port = 50051

# Create a subclass of QMainWindow to setup the GUI
class RfsaUi(QMainWindow):
    """PyCalc's View (GUI)."""
    def __init__(self):
        """View initializer."""
        super().__init__()
        # Set some main window's properties
        self.setWindowTitle('RFSA Spectrum vi Front Panel')
        self.setFixedSize(1000, 600)
        # Set the central widget and the general layout
        self.generalLayout = QHBoxLayout()
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)
        # Create the display and the buttons
        self._createLayout()

    def closeEvent(self, event):
        #close current session:
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self.resourceName.currentText()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/closesession", parameters)
        conn.close()
        

    def _createLayout(self):
        self.centralLayout = QVBoxLayout()
        self.resourceName = QComboBox()
        self.clockSource = QComboBox()
        self.resourceNameLabel = QLabel()
        self.resourceNameLabel.setText("Resource Name")
        self.clockSourceLabel = QLabel()
        self.clockSourceLabel.setText("Clock Source")
        self.referenceLevelLabel = QLabel()
        self.referenceLevelLabel.setText("Reference Level(dBm)")
        self.centerFrequencyLabel = QLabel()
        self.centerFrequencyLabel.setText("Center Frequency(Hz)")
        self.spanLabel = QLabel()
        self.spanLabel.setText("Span(Hz)")
        self.rbwLabel = QLabel()
        self.rbwLabel.setText("RBW(Hz)")
        self.numAvgsLabel = QLabel()
        self.numAvgsLabel.setText("# Avgs")

        self.refLevelInput = QLineEdit()
        self.centerFreqInput = QLineEdit()
        self.spanInput = QLineEdit()
        self.rbwInput = QLineEdit()
        self.numAvgsInput = QLineEdit()

        self.errorLabel = QLabel()
        self.errorLabel.setText("error:")
        self.errorOut = QLabel()
        self.errorOut.setMinimumSize(75,75)
        self.errorOut.setWordWrap(True)
        self.errorOut.setAlignment(Qt.AlignLeft)
        self.stopButton = QPushButton("STOP")
        self.closeSessionsButton = QPushButton("Close All VST Sessions")

        self.centralLayout.addWidget(self.resourceNameLabel)
        self.centralLayout.addWidget(self.resourceName)
        self.centralLayout.addWidget(self.clockSourceLabel)
        self.centralLayout.addWidget(self.clockSource)
        self.centralLayout.addWidget(self.referenceLevelLabel)
        self.centralLayout.addWidget(self.refLevelInput)
        self.centralLayout.addWidget(self.centerFrequencyLabel)
        self.centralLayout.addWidget(self.centerFreqInput)
        self.centralLayout.addWidget(self.spanLabel)
        self.centralLayout.addWidget(self.spanInput)
        self.centralLayout.addWidget(self.rbwLabel)
        self.centralLayout.addWidget(self.rbwInput)
        self.centralLayout.addWidget(self.numAvgsLabel)
        self.centralLayout.addWidget(self.numAvgsInput)
        self.centralLayout.addWidget(self.errorLabel)
        self.centralLayout.addWidget(self.errorOut)
        self.centralLayout.addWidget(self.stopButton)
        self.centralLayout.addWidget(self.closeSessionsButton)
        self.generalLayout.addLayout(self.centralLayout)
        self.centerFreqInput.setText("100000000")
        self.refLevelInput.setText("0")
        self.spanInput.setText("20000000")
        self.rbwInput.setText("100000")
        
        #fill up resourceName comboBox
        conn = http.client.HTTPConnection(ip, port)
        conn.request("GET", f"/niRFSA/vstlist")
        response = conn.getresponse().read().decode().split(sep = "\n")
        self.resourceName.addItems(response)
        conn.close()

        self.clockSource.addItems(['PXI_CLK', 'OnboardClock', 'ClkIn', 'RefIn'])

        #graph
        self.graphWidget = pg.PlotWidget()
        hour = [1,2,3,4,5,6,7,8,9,10]
        temperature = [30,32,34,32,33,31,29,32,35,45]
        # plot data: x, y values
        self.graphWidget.plot(hour, temperature)
        self.graphWidget.setLabel(axis='left', text='Power(dBm)')
        self.graphWidget.setLabel(axis='bottom', text='Frequency(Hz)')
        self.generalLayout.addWidget(self.graphWidget)

        #timer to update graph every 25 ms
        self.timer=QTimer()
        #self.timer.start(25)



# Create a Controller class to connect the GUI and the model
class RfsaCtrl:
    """Controller class."""
    def __init__(self, view):
        """Controller initializer."""
        self._view = view
        # Connect signals and slots
        self._connectSignals()
        self._init_rfsa()
        self._config_clock_source()
        self._config_reference_level()
        self._config_center_frequency()
        self._config_span()
        self._config_rbw()

    def _init_rfsa(self):
        conn = http.client.HTTPConnection(ip, port)
        #initialize rfsg with all the expected parameters with the specific name
        parameters = {'resourceName': self._view.resourceName.currentText(), 'idQuery':True, 'reset':False}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/opensession", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        #cause event every 50 ms
        self._view.timer.start(50)

        #configure reference clock to PXI CLOCK & Configure acquisition type to spectrum.
        parameters = {'resourceName': self._view.resourceName.currentText(), 'refClockSource':'PXI_CLK', 'acquisitionType' : 101}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_clock_source(self):
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self._view.resourceName.currentText(), 'refClockSource':self._view.clockSource.currentText()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_reference_level(self):
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self._view.resourceName.currentText(), 'referenceLevel':self._view.refLevelInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_center_frequency(self):
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self._view.resourceName.currentText(), 'centerFrequency':self._view.centerFreqInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_span(self):
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self._view.resourceName.currentText(), 'spectrumSpan':self._view.spanInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_rbw(self):
        conn = http.client.HTTPConnection(ip, port)
        parameters = {'resourceName': self._view.resourceName.currentText(), 'resolutionBandwidth':self._view.rbwInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

    def _config_num_avgs(self):
        return

    def _update_plot(self):
        conn = http.client.HTTPConnection(ip, port)
        #attempt read:
        attrread = {'resourceName': self._view.resourceName.currentText(), 'channelList':'', 'timeout':10}
        parameters = json.dumps(attrread)
        conn.request("POST", f"/niRFSA/read", parameters)
        powerSpectrumData = conn.getresponse().read()
        try:
            powerSpectrumData = numpy.frombuffer(powerSpectrumData, dtype = "float64")
        except:
            self._view.errorOut.setText(powerSpectrumData.decode("utf-8"))
            conn.close()
            return
        interval = ((float(self._view.spanInput.text())/len(powerSpectrumData)))
        halfSpan = int(int(self._view.spanInput.text())/2)
        xAxis = numpy.arange(-halfSpan, halfSpan, interval).tolist()
        #plot the data
        self._view.graphWidget.plot(xAxis, powerSpectrumData, clear = True)

        self._config_reference_level()
        self._config_center_frequency()
        self._config_span()
        self._config_rbw()
        
        conn.close()

    def _stop(self):
        self._view.timer.stop()
        conn = http.client.HTTPConnection(ip, port)
        #close session on device
        parameters = {'resourceName': self._view.resourceName.currentText()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSA/closesession", parameters)
        error = conn.getresponse().read().decode()
        if(error != ''):
            conn.close()
            return
        conn.close()
    def _close_all_sessions(self):
        conn = http.client.HTTPConnection(ip, port)
        conn.request("POST", f"/closeallsessions")
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()

        
    def _connectSignals(self):
        """Connect signals and slots."""
        self._view.resourceName.activated.connect(self._init_rfsa)
        self._view.clockSource.activated.connect(partial(self._config_clock_source))
        self._view.refLevelInput.textEdited.connect(partial(self._config_reference_level))
        self._view.centerFreqInput.textEdited.connect(partial(self._config_center_frequency))
        self._view.spanInput.textEdited.connect(partial(self._config_span))
        self._view.rbwInput.textEdited.connect(partial(self._config_rbw))
        self._view.numAvgsInput.textEdited.connect(partial(self._config_num_avgs))
        self._view.stopButton.clicked.connect(self._stop)
        self._view.timer.timeout.connect(self._update_plot)
        self._view.closeSessionsButton.clicked.connect(self._close_all_sessions)
   


# Client code
def main():
    """Main function."""
    # Create an instance of `QApplication`
    rfsaGUI = QApplication(sys.argv)
    
    view = RfsaUi()
    view.show()

    RfsaCtrl(view=view)

    sys.exit(rfsaGUI.exec_())

if __name__ == '__main__':
    main()
