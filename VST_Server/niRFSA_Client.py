import http.client;
import json;
import bson;
import numpy;
import sys;


class niRFSA_Client():
    def __init__(self, resourceName, conn):
        self.resourceName = resourceName
        self.conn = conn
    
    def get_device_list(self):
        self.conn.request("GET", f"/niRFSA/vstlist")
        response = self.conn.getresponse().read().decode().split(sep = "\n")
        return response

    def open_session(self, IDQuery, resetDevice, optionString):
        parameters = {'resourceName': self.resourceName, 'idQuery':IDQuery, 'reset':resetDevice, 'optionString': optionString}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSA/opensession", parameters)
        return self.conn.getresponse().read().decode()

    def close_session(self):
        parameters = {'resourceName': self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSA/closesession", parameters)
        return self.conn.getresponse().read().decode()

    def abort(self):
        parameters = {'resourceName' : self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSA/abort", parameters)
        return self.conn.getresponse().read().decode()

    def initiate(self):
        parameters = {'resourceName' : self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSA/initiate", parameters)
        return self.conn.getresponse().read().decode()

    #currently closes all niRFSG and niRFSA devices.
    def close_all_sessions(self):
        self.conn.request("POST", f"/closeallsessions")
        return self.conn.getresponse().read().decode()


    def wait_until_settled(self, maxTimeMilliseconds):
        parameters = {'resourceName': self.resourceName, 'maxTimeMilliseconds':maxTimeMilliseconds}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSA/waituntilsettled", parameters)
        return self.conn.getresponse().read().decode()

    def configure(self, data):
        data['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSA/configure", json.dumps(data))
        return self.conn.getresponse().read().decode()

    def get_configuration(self, data_list):
        data_list['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSA/getconfiguration", json.dumps(data_list))
        return self.conn.getresponse().read().decode()
    
    def testbinary(self, header, data):
        self.conn.request("POST", f"/testbinary", data, header)
        return self.conn.getresponse().read()

    def fetch(self, parameters):
        parameters['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSA/fetch", json.dumps(parameters))
        iqdata = self.conn.getresponse().read()
        IQ = numpy.frombuffer(iqdata, dtype = "float32")
        I, Q = IQ[::2], IQ[1::2]
        #currently does not return anything. Just prints I and Q data but easy to alter
        print(I)
        print(Q)
        return 0

    def read(self, parameters):
        parameters['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSA/read", json.dumps(parameters))
        powerSpectrumData = self.conn.getresponse().read()
        powerSpectrumData = numpy.frombuffer(powerSpectrumData, dtype = "float64")
        return powerSpectrumData



if __name__ == '__main__':
    #use top connection for IP connection on port 50051
    #conn = http.client.HTTPConnection('10.190.1.181', 50051)
    conn = http.client.HTTPConnection('localhost', 50051)

    #initialize an niRFSA_Client object with resourceName = "resourceName"
    api = niRFSA_Client("resourceName", conn)

    #Print out a list of available RFSA Devices
    print(api.get_device_list())

    #open a session on the device that is a simulated 5644R VST(same type Genx uses)
    api.open_session(True, False, "Simulate=1, DriverSetup=Model:5644R")

    #initiate the session
    api.initiate()

    #get attribute values
    desiredAttributes = {'attributes':['numberOfSamples', 'acquisitionType', 'spectrumSpan', 'numberOfSpectralLines', 'resolutionBandwidth']}
    test = api.get_configuration(desiredAttributes)
    print(test)

    #Fetch example
    attrfetch = {'type' : 'multiRecord', 'dataType':'F32', 'channelList': '', 'startingRecord':0, 'numberOfRecords': 1, 'numberOfSamples':300, 'timeout': 60}
    test = api.fetch(attrfetch)
    
    #configure acquisitionType
    rfConfig = {'acquisitionType':101}
    print(api.configure(rfConfig))

    #get attribute values again
    desiredAttributes = {'attributes':['numberOfSamples', 'acquisitionType', 'spectrumSpan', 'numberOfSpectralLines', 'resolutionBandwidth']}
    test = api.get_configuration(desiredAttributes)
    print(test)


    #read example
    attrread = {'channelList':'', 'timeout':60}
    test = api.read(attrread)
    print(test)

    #example of sending and receiving F32 bytes
    header = {"numberOfSamples":6}
    data = [1.3, 423.3, 7.112, 34, 23.12, 0]
    #make numpy array for f32 of data
    data2 = numpy.array(data, dtype = "float32")
    #ensure little endian
    if sys.byteorder=='big':
        test2 = data2.byteswap()
    #convert to bytes
    data2 = data2.tobytes()
    #testbinary sends the bytes to the server, reads them into float array and converts back to bytes to send back
    test = api.testbinary(header, data2)
    #convert from bytes back to numpy array(human readable)
    test = numpy.frombuffer(test, dtype = "float32")
    print(test)

    #abort session
    api.abort()

    #close session
    api.close_session()
    
    
    #close connection
    conn.close()
        
        
