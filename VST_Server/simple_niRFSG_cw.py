import sys
import http.client
import json

# Import QApplication and the required widgets from PyQt5.QtWidgets
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QGridLayout
from PyQt5.QtWidgets import QLineEdit
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QVBoxLayout
from PyQt5.QtWidgets import QComboBox
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QFormLayout
from PyQt5.QtWidgets import QLabel
from functools import partial


ip = 'localhost'
port = 50051

# Create a subclass of QMainWindow to setup the GUI
class RfsgUi(QMainWindow):
    """PyCalc's View (GUI)."""
    def __init__(self):
        """View initializer."""
        super().__init__()
        # Set some main window's properties
        self.setWindowTitle('Simple rfsg cw front panel')
        self.setFixedSize(450, 450)
        # Set the central widget and the general layout
        self.generalLayout = QVBoxLayout()
        self._centralWidget = QWidget(self)
        self.setCentralWidget(self._centralWidget)
        self._centralWidget.setLayout(self.generalLayout)
        # Create the display and the buttons
        self._createLayout()
    def closeEvent(self, event):
        #close current session:
        conn = http.client.HTTPConnection(ip, port)
        #configure outputEnabled to false
        parameters = {'resourceName': self.resourceName.currentText()}
        parameters = json.dumps(parameters)
        #conn.request("POST", f"/niRFSG/configure", parameters)
        conn.request("POST", f"/niRFSG/closesession", parameters)
        conn.close()

        
        event.accept()
        sys.exit()
        

    def _createLayout(self):
        #rfsg label and input VST name
        self.centralLayout = QHBoxLayout()
        self.resourceName = QComboBox()
        self.rfsgLabel = QLabel()
        self.rfsgLabel.setText("rfsg")
        self.centralLayout.addWidget(self.rfsgLabel)
        self.centralLayout.addWidget(self.resourceName)
        self.generalLayout.addLayout(self.centralLayout)

        #input output with labels
        self.inputLayout = QFormLayout()
        self.freqInput = QLineEdit()
        self.inputLayout.addRow("freq(Hz)",self.freqInput)
        self.ucfInput = QLineEdit()
        self.inputLayout.addRow("UCF",self.ucfInput)
        self.deltaInput = QLineEdit()
        self.inputLayout.addRow("delta",self.deltaInput)
        self.dBmInput = QLineEdit()
        self.inputLayout.addRow("dBm",self.dBmInput)
        self.phaseInput = QLineEdit()
        self.inputLayout.addRow("phase",self.phaseInput)

        self.freqInput.setText("100000000")
        self.dBmInput.setText("0")
        self.phaseInput.setText("0")
        self.ucfInput.setText("99999999.985098839")
        self.deltaInput.setText("0.014901161193847656")
        
        self.generalLayout.addLayout(self.inputLayout)

        #dropdown menu, and vertical layout with stop button and error box
        self.rightLayout = QVBoxLayout()
        self.stopButton = QPushButton("STOP")
        self.rightLayout.addWidget(self.stopButton)
        self.closeSessionsButton = QPushButton("Close All VST Sessions")
        self.rightLayout.addWidget(self.closeSessionsButton)
        self.errorLabel = QLabel()
        self.errorLabel.setText("error:")
        self.rightLayout.addWidget(self.errorLabel)
        self.errorOut = QLabel()
        self.errorOut.setMinimumSize(75,75)
        self.errorOut.setWordWrap(True)
        self.errorOut.setAlignment(Qt.AlignLeft)
        self.rightLayout.addWidget(self.errorOut)
        self.generalLayout.addLayout(self.rightLayout)

        #fill up resourceName comboBox
        conn = http.client.HTTPConnection(ip, port)
        conn.request("GET", f"/niRFSG/vstlist")
        response = conn.getresponse().read().decode().split(sep = "\n")
        self.resourceName.addItems(response)
        conn.close()



# Create a Controller class to connect the GUI and the model
class RfsgCtrl:
    """Controller class."""
    def __init__(self, view):
        """Controller initializer."""
        self._view = view
        # Connect signals and slots
        self._connectSignals()
        #initialize rfsg with initial state
        self._init_rfsg()

   
    def _init_rfsg(self):
        conn = http.client.HTTPConnection(ip, port)
        #initialize rfsg with all the expected parameters with the specific name
        parameters = {'resourceName': self._view.resourceName.currentText(), 'idQuery':True, 'reset':False}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/opensession", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return

        #configure reference clock to PXI CLOCK
        parameters = {'resourceName': self._view.resourceName.currentText(), 'refClockSource':'PXI_CLK'}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return

        #initiate it
        parameters = {'resourceName' : self._view.resourceName.currentText()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/initiate", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        self._config_freq()
        self._config_dBm()
        self._config_phase()
        
        conn.close()


    def _config_freq(self):
        conn = http.client.HTTPConnection(ip, port)
        #configure the frequency and update error text
        #possibly update the UCF+ as well
        parameters = {'resourceName': self._view.resourceName.currentText(), 'frequency':self._view.freqInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/configure", parameters)
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return

        #get and update UCF and delta
        parameters = {'resourceName': self._view.resourceName.currentText(), 'attributes':['frequency', 'upconverterCenterFrequency']}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/getconfiguration", parameters)
        error = conn.getresponse().read().decode()
        try:
            error = json.loads(error)
            freq = error.get('frequency')
            UCF = error.get('upconverterCenterFrequency')
            self._view.ucfInput.setText(UCF)
            delta = float(freq)-float(UCF)
            self._view.deltaInput.setText(str(delta))
        except:
            self._view.errorOut.setText(error)
            if(error != ''):
                conn.close()
                return

        conn.close()
        
    def _config_dBm(self):
        conn = http.client.HTTPConnection(ip, port)
        #configure the dBm and update error text
        #possibly update the UCF+ as well
        parameters = {'resourceName': self._view.resourceName.currentText(), 'powerLevel':self._view.dBmInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/configure", parameters)
        error = conn.getresponse().read().decode()
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        conn.close()

    def _config_phase(self):
        conn = http.client.HTTPConnection(ip, port)
        #configure the phase and update error text
        #possibly update the UCF+ as well
        parameters = {'resourceName': self._view.resourceName.currentText(), 'phaseOffset':self._view.phaseInput.text()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/configure", parameters)
        error = conn.getresponse().read().decode()
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        conn.close()
  
    def _stop(self):
        conn = http.client.HTTPConnection(ip, port)
        #configure outputEnabled to false
        parameters = {'resourceName': self._view.resourceName.currentText(), 'outputEnabled':False}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/configure", parameters)
        error = conn.getresponse().read().decode()
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return

        #close session on device
        parameters = {'resourceName': self._view.resourceName.currentText()}
        parameters = json.dumps(parameters)
        conn.request("POST", f"/niRFSG/closesession", parameters)
        error = conn.getresponse().read().decode()
        if(error != ''):
            conn.close()
            return
        conn.close()
    def _close_all_sessions(self):
        conn = http.client.HTTPConnection(ip, port)
        conn.request("POST", f"/closeallsessions")
        error = conn.getresponse().read().decode()
        #check error
        self._view.errorOut.setText(error)
        if(error != ''):
            conn.close()
            return
        
        conn.close()
        
    def _connectSignals(self):
        """Connect signals and slots."""
        self._view.resourceName.activated.connect(self._init_rfsg)
        self._view.freqInput.textEdited.connect(partial(self._config_freq))
        self._view.dBmInput.textEdited.connect(partial(self._config_dBm))
        self._view.phaseInput.textEdited.connect(partial(self._config_phase))
        self._view.stopButton.clicked.connect(self._stop)
        self._view.closeSessionsButton.clicked.connect(self._close_all_sessions)
        
        

# Client code
def main():
    """Main function."""
    # Create an instance of `QApplication`
    rfsgGUI = QApplication(sys.argv)
    
    view = RfsgUi()
    view.show()

    RfsgCtrl(view=view)

    sys.exit(rfsgGUI.exec_())

if __name__ == '__main__':
    main()
