import http.client;
import json;
import bson;
import numpy;
import sys;

class niRFSG_Client():
    def __init__(self, resourceName, conn):
        self.resourceName = resourceName
        self.conn = conn
    
    def get_device_list(self):
        self.conn.request("GET", f"/niRFSG/vstlist")
        response = self.conn.getresponse().read().decode().split(sep = "\n")
        return response

    def open_session(self, IDQuery, resetDevice, optionString):
        parameters = {'resourceName': self.resourceName, 'idQuery':IDQuery, 'reset':resetDevice, 'optionString': optionString}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSG/opensession", parameters)
        return self.conn.getresponse().read().decode()

    def close_session(self):
        parameters = {'resourceName': self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSG/closesession", parameters)
        return self.conn.getresponse().read().decode()

    def abort(self):
        parameters = {'resourceName' : self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSG/abort", parameters)
        return self.conn.getresponse().read().decode()

    def initiate(self):
        parameters = {'resourceName' : self.resourceName}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSG/initiate", parameters)
        return self.conn.getresponse().read().decode()

    #currently closes all niRFSG and niRFSA devices
    def close_all_sessions(self):
        self.conn.request("POST", f"/closeallsessions")
        return self.conn.getresponse().read().decode()


    def wait_until_settled(self, maxTimeMilliseconds):
        parameters = {'resourceName': self.resourceName, 'maxTimeMilliseconds':maxTimeMilliseconds}
        parameters = json.dumps(parameters)
        self.conn.request("POST", f"/niRFSG/waituntilsettled", parameters)
        return self.conn.getresponse().read().decode()

    def configure(self, data):
        data['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSG/configure", json.dumps(data))
        return self.conn.getresponse().read().decode()

    def get_configuration(self, data_list):
        data_list['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSG/getconfiguration", json.dumps(data_list))
        return self.conn.getresponse().read().decode()
    
    def testjson(self, data):
        self.conn.request("POST", f"/simplejson", json.dumps(data))
        return self.conn.getresponse().read().decode()
    
    def testbinary(self, header, data):
        self.conn.request("POST", f"/testbinary", data, header)
        return self.conn.getresponse().read()

    def writeScript(self, parameters):
        parameters['resourceName'] = self.resourceName
        self.conn.request("POST", f"/niRFSG/writescript", json.dumps(parameters))
        result = self.conn.getresponse().read()
        return result

    def write(self, i, q, header):
        iq = i+q
        data = numpy.array(iq, dtype = "float32")
        data = data.tobytes()
        self.conn.request("POST", f"/niRFSG/write", data, header)
        return self.conn.getresponse().read().decode()
        



if __name__ == '__main__':
    #usew top connection for IP
    #conn = http.client.HTTPConnection('10.190.1.181', 50051)
    conn = http.client.HTTPConnection('localhost', 50051)
    #initializing RFSG object with name 'resourceName'
    api = niRFSG_Client("resourceName", conn)
    #set up VST in simulation mode and open session
    api.open_session(True, False, "Simulate=1, DriverSetup=Model:5644R")
    #print open devices list
    print(api.get_device_list())

    
    #configure RF
    rfConfig = {'frequency':1500000000, 'powerLevel': 1}
    print(api.configure(rfConfig))

    #test get attributes
    desiredAttributes = {'attributes':['upconverterCenterFrequency', 'frequency', 'powerLevel', 'refClockRate', 'startTriggerType']}
    test = api.get_configuration(desiredAttributes)
    print(test)

    #Write script example
    scriptattr = {'script':'''script generateWaveformWithMarkers
   repeat forever
      repeat 5
      generate signal marker0(0) marker1(0)
      end repeat
      repeat 2
      generate quiet
      end repeat
   end repeat
end script'''}
    test = api.writeScript(scriptattr)


    #write example
    header = {'resourceName':'resourceName', 'waveformName':'wftest', 'numberOfSamples': 11, 'moreDataPending':1}
    i = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    q = [0, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
    test = api.write(i, q, header)
    print("test write:" + test)

    #close session
    api.close_session()
    
    
    
    conn.close()
        
        
